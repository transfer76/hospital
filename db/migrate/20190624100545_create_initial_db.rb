class CreateInitialDb < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
    	t.string :username

    	t.timestamps
    end
    add_index :patients, :username, unique: true

    create_table :doctors do |t|
    	t.string :username
      t.string :specialty
      t.string :address

      t.timestamps
    end

  create_table :appointments do |t|
    	t.string :appointment_date
    	t.references :doctor, foreign_key: true
    	t.references :patient, foreign_key: true

    	t.timestamps
    end
  end
end
