Rails.application.routes.draw do
  devise_for :patients
	root to: "doctors#index"

	resources :doctors
	resources :appointments
	resources :patients
end
