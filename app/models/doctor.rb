class Doctor < ApplicationRecord
	has_many :appointments
	has_many :patients, through: :appointments

	validates :username, :address, :specialty, presence: true
end
